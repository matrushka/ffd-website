(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.languageSwitcher = {
    attach: function (context, settings) {
      var $languageSwitcherToggle = $(".language-switcher__toggle-button", context);
      var $languageSwitcherContainer = $(".language-switcher__dropdown", context);

      if($languageSwitcherToggle.length > 0 && $languageSwitcherContainer.length > 0) {
  
        $languageSwitcherToggle.click(function(e){
          e.preventDefault();
          $languageSwitcherToggle.toggleClass('is-active');
          $languageSwitcherContainer.toggleClass('is-hidden');
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings);