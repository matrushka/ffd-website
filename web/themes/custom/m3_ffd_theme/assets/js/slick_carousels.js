(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.slickCarousels = {
    attach: function (context, settings) {

      var $slickCarouselsMultiple = $('.slick-carousel--multiple', context);
      
      if($slickCarouselsMultiple.length) {
        $slickCarouselsMultiple.slick({
          slidesToShow: 5,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 4000,
          centerMode: true,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 786,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1
              }
            },
          ]
        });
      }

      var $slickCarouselsSingle = $('.slick-carousel--single', context);

      if($slickCarouselsSingle.length) {
        $slickCarouselsSingle.slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
          autoplay: true,
          autoplaySpeed: 4000,
          appendArrows: $('.slick-carousel__controls'),
          appendDots: $('.slick-carousel__controls'),
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings);