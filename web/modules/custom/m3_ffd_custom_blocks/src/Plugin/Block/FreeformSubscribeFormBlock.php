<?php

namespace Drupal\m3_ffd_custom_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Subscribe Form' Block code to implement a freeform based block for Subscribe form.
 *
 * @Block(
 *   id = "freeform_subscribe_form_block",
 *   admin_label = @Translation("Subscribe Form"),
 *   category = @Translation("ffd Custom Blocks"),
 * )
 */
class FreeformSubscribeFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $code = 'subscribeform';
    return array(
      '#theme' => 'freeform',
      '#code' => $code,
    );
  }

}
