<?php

/**
 * @file
 * Contains m3_ffd_year_filter.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\Cache;

/**
 * Implements hook_help().
 */
function m3_ffd_year_filter_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the m3_ffd_year_filter module.
    case 'help.page.m3_ffd_year_filter':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('ffd M3 custom year exposed filter option list generators and validators; provides a year list for this exposed filter.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function m3_ffd_year_filter_form_views_exposed_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (isset($form['#id']) && ($form['#id'] == 'views-exposed-form-search-page' || $form['#id'] == 'views-exposed-form-resources-page')) {
    $options = &drupal_static(__FUNCTION__);
    if (is_null($options)) {

      switch($form['#id']) {
        case 'views-exposed-form-search-page':
          $cid = 'm3_ffd_year_filter:search_page:year';
          $data = \Drupal::cache()->get($cid);
          if (!$data) {
            $options = [];
            $options[''] = t('- All -');
            $index = \Drupal\search_api\Entity\Index::load('general_search');
            $query = $index->query();
            $query->sort('created', 'ASC');
            $results = $query->execute();
            if ($results) {
              $items = $results->getResultItems();
              foreach ($items as $item) {
                $date = $item->getField('created')->getValues();
                if ($date) {
                  $year = date('Y', $date[0]);
                  if (!isset($options[$year])) {
                    $options[$year] = $year;
                  }
                }
              }
            }
            $cache_tags = ['m3_ffd_year_filter:search_page:year'];
            \Drupal::cache()->set($cid, $options, CacheBackendInterface::CACHE_PERMANENT, $cache_tags);
          } else {
            $options = $data->data;
          }
        break;
        case 'views-exposed-form-resources-page':
          $cid = 'm3_ffd_year_filter:resources_page:year';
          $data = \Drupal::cache()->get($cid);
          if (!$data) {
            $options = [];
            $options[''] = t('- All -');
            $index = \Drupal\search_api\Entity\Index::load('general_search');
            $query = $index->query();
            $query->addCondition('type', 'resource')
              ->sort('created', 'ASC');
            $results = $query->execute();
            if ($results) {
              $items = $results->getResultItems();
              foreach ($items as $item) {
                $date = $item->getField('created')->getValues();
                if ($date) {
                  $year = date('Y', $date[0]);
                  if (!isset($options[$year])) {
                    $options[$year] = $year;
                  }
                }
              }
            }
            $cache_tags = ['m3_ffd_year_filter:resources_page:year'];
            \Drupal::cache()->set($cid, $options, CacheBackendInterface::CACHE_PERMANENT, $cache_tags);
          } else {
            $options = $data->data;
          }
        break;
      }
    }
    
    $form['year'] = [
      '#title' => t('Year'),
      '#type' => 'select',
      '#options' => $options,
      '#size' => NULL,
      '#default_value' => '- All -',
    ];
    
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function m3_ffd_year_filter_node_presave(EntityInterface $entity) {
  // Check if a node updated has a new year, and invalidate the
  // options cached used in the custom views filter for filtering by year.
  $bundle = $entity->bundle();
  $cid = 'm3_ffd_year_filter:search_page:year';
  $data = \Drupal::cache()->get($cid);
  if ($data) {
    $options = $data->data;
    $date = $entity->get('created')->first()->getValue();
    if ($date) {
      $year = date('Y', $date['value']);
      if (!isset($options[$year])) {
        Cache::invalidateTags(['m3_ffd_year_filter:search_page:year']);
      }
    }
  }
  // In case the Node is Resource, check and invalidate their own list too
  if($bundle == "resource") {
    $cid = 'm3_ffd_year_filter:resources_page:year';
    $data = \Drupal::cache()->get($cid);
    if ($data) {
      $options = $data->data;
      $date = $entity->get('created')->first()->getValue();
      if ($date) {
        $year = date('Y', $date['value']);
        if (!isset($options[$year])) {
          Cache::invalidateTags(['m3_ffd_year_filter:resources_page:year']);
        }
      }
    }
  }
}