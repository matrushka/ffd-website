## About

Startup files and folder recipe for M-III based Drupal projects. Uses Drupal 9 and a set of default content types and modules.

### What is included

- Composer recipe and folder structure for **Drupal 9 recommended project**
- Configuration in composer recipe to load **M-III** dependencies and configurations
- DDev recipe to start the project with **DDev+Docker**
- Custom commands for DDev to initialize a new project based on this boilerplate
- Optional, custom commands for DDev to initialize a boilerplate instance for its own development

### What is recommended to have in your dev envirotment

- Docker service
- DDev-Local tools

### Getting started

- After cloning this repository, run `ddev config` to make update project name.
- Run `ddev start` to create the containers required for the project and execute the configured post-start steps.
- Run `ddev install` after containers started for the first time. It will install and prepare the site as a new project, decoupled from Boilerplate repo. It will also install all drupal database tables and do all settings required for the site to work as expected. **IMPORTANT!** Just run this once in a project's life, since it will replace the **.git** and **custom theme** folders, and any changes not saved could be lost.
- Enjoy your Drupal M-III boosted site :)

### Drupal site automatic configuration

When site is installed as expected in the previous steps, it will have this configuration:

- Site mail: web-admin@matrushka.com.mx
- (Main) Account Name: admin
- (Main) Account Password: autogenerated, check command prompts for the key
- (Main) Account Mail: web-admin@matrushka.com.mx
- Default country: none
- Default time zone: UTC

Also:
- A theme cloned from M3 startup template will be created and configured by default.
- Template Modules for M3 recommended functions will be cloned in web/modules/custom, but will not be configured.

### Not starting a new project, but wanting to contribute to this boilerplate development?

If that is the case, run `ddev install-dev` instead of `ddev install`, to prevent git decoupling but do all installation and configuration steps required for the site to work as expected.